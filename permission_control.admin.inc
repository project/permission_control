<?php

/**
 * Menu callback: config of administer permissions.
 *
 * @ingroup forms
 * @see permission_control_config_submit()
 * @see theme_permission_control_config()
 */
function permission_control_config($form_state, $rid = NULL) {
  list($lid_arr, $translation_arr) = _get_perm_arr();
  foreach (module_list(FALSE, FALSE, TRUE) as $module) {
    if ($permissions = module_invoke($module, 'perm')) {
      $form['permission'][] = array(
        '#value' => $module,
      );
      asort($permissions);
      foreach ($permissions as $perm) {
      	$perm = str_replace(" ", "_", $perm);
//      	print $perm;
        $form['permission'][$perm] = array('#value' => t($perm));
        $form['checkboxes'][$perm .'_ch'] = array('#type' => 'checkbox', '#default_value' => ($lid_arr[$perm] == 1) ? 1 : 0);
        $form['textfields'][$perm .'_tx'] = array('#type' => 'textfield', '#default_value' => ($translation_arr[$perm] != '') ? $translation_arr[$perm] : '');
      }
    }
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save permissions'));

  return $form;
}

/**
 * Implemetaion of hook_form_submit
 */
function permission_control_config_submit($form, &$form_state) {
  // Save permissions config:
  db_query('DELETE FROM {perm_control}');
  $post_arr = $form_state['clicked_button']['#post'];
  foreach ($post_arr as $k => $v) {
  	if (preg_match("/(.*)_tx$/", $k, $args)) {
  		$permissions = $args[1];
  		db_query('INSERT INTO {perm_control} VALUES("%s", %d, "%s")', $permissions, $post_arr[$permissions .'_ch'], $post_arr[$permissions .'_tx']);
  	}
  }
  global $user;
  watchdog('permission', 'Permission list configuration is changed by %name.', array('%name' => $user->name));
  drupal_set_message(t('The changes have been saved.'));
}

//function $list_arr = _get_list_arr();

/**
 * Theme the administer permissions page.
 *
 * @ingroup themeable
 */
function theme_permission_control_config($form) {
  $roles = user_roles();
  foreach (element_children($form['permission']) as $key) {
    // Don't take form control structures
    if (is_array($form['permission'][$key])) {
      $row = array();
      // Module name
      if (is_numeric($key)) {
        $row[] = array('data' => t('@module module', array('@module' => drupal_render($form['permission'][$key]))), 'class' => 'module', 'id' => 'module-'. $form['permission'][$key]['#value'], 'colspan' => count($form['role_names']) + 3);
      }
      else {
        $form['permission'][$key] = _remove_underscore($form['permission'][$key]);
        $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => 'permission');
        $row[] = array('data' => drupal_render($form['checkboxes'][$key .'_ch']), 'class' => 'checkbox');
        $row[] = array('data' => drupal_render($form['textfields'][$key .'_tx']), 'class' => 'textfields');
      }
      $rows[] = $row;
    }
  }
  $header = array(t('Permission'), t('Permission list'), t('Translation'));
  $output = theme('table', $header, $rows, array('id' => 'permissions'));
  $output .= drupal_render($form);
  return $output;
}

function _remove_underscore($str) {
  return str_replace("_", " ", $str);
}
